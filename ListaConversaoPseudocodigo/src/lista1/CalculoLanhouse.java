package lista1;
import java.util.Scanner;

public class CalculoLanhouse {
	public static void main(String[] args) {
		float cobranca, consumo, valorPago;
		cobranca = 2.3f;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Quantidade de minutos de consumo: ");
		consumo = sc.nextFloat();
		
		valorPago = (consumo / 60) * cobranca;
		
		System.out.println("O valor a ser pago �: " + valorPago);
		
		sc.close();
	}
}


/*
Var
cobranca: real
minutos: real
valor: real

Inicio
escreva("Quantidade de minutos de consumo: ")
leia(minutos)

cobranca <- 2.3
valor <- (minutos/60)*cobranca

escreva("O valor a ser pago �: R$ ", valor)

Fimalgoritmo
*/