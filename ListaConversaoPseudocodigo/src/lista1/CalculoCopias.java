package lista1;
import java.util.Scanner;

public class CalculoCopias {
	
	public static void main(String[] args) {
		double qtdeFolhas, total, valorCopia = 0.08;
		int frenteVerso = 2;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Quantidade de folhas: ");
		qtdeFolhas = sc.nextDouble();
		
		total = qtdeFolhas * valorCopia * frenteVerso;
		
		System.out.println("O valor a ser pago �: R$ " + total);
		
		sc.close();
	}
}



/*
Algoritmo "ex 3 - calculo valor de c�pias"
Var
valor_copia: real
qtde_folhas: real
valor: real

Inicio
escreva("Quantidade de folhas:")
leia(qtde_folhas)
valor_copia <- 0.08

valor <- qtde_folhas*2*valor_copia

escreva("O valor a ser pago �: R$ ",valor)

Fimalgoritmo
*/