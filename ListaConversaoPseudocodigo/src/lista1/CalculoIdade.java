package lista1;
import java.util.*;

public class CalculoIdade {
	public static void main(String[] args) {
        int anoAtual, anoNasc, idade;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ano atual: ");
        anoAtual = sc.nextInt();
        System.out.print("Ano de Nacimento: ");
        anoNasc = sc.nextInt();
        
        idade = anoAtual - anoNasc;
        System.out.println("Sua idade �: " + idade);
        
        sc.close();
    }
}




/*
Algoritmo "ex 1 - calculo idade"
Var
ano_atual: inteiro
ano_nasc: inteiro
idade: inteiro

Inicio
escreva("Ano atual: ")
leia(ano_atual)
escreva("Ano de nascimento: ")
leia(ano_nasc)

idade <- ano_atual - ano_nasc

escreva("Sua idade �: ", idade)

Fimalgoritmo

*/