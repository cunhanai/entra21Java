package lista3;

public class CatorzeB {

	public static void main(String[] args) {
		int a, b, c;
		
		a = 30;
		b = 20;
		c = a + b;
		System.out.println(c);
		
		b = 10;
		System.out.println(b + ", " + c);
		
		c = a + b;
		System.out.println(a + ", " + b + ", " + c);
	}

}


/*
Algoritmo "ex 14.b)"
Var
   a, b, c: inteiro
Inicio
   a <- 30
   b <- 20
   c <- a + b
   escreval(c)
   
   b <- 10
   escreval(b, ",", c)
   
   c <- a + b
   escreval(a,",", b,",", c)
Fimalgoritmo
*/