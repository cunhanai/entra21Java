package lista3;

public class CatorzeE {
	
	public static void main(String[] args) {
		int a, b, c;
		
		a = 10;
		b = 5;
		c = a + b;
		b = 20;
		a = 10;
		
		System.out.println(a + ", " + b + ", " + c);
	}
}

/*
Algoritmo "ex 14.e)"
Var
   a, b, c: inteiro
Inicio
   a <- 10
   b <- 5
   c <- a + b
   b <- 20
   a <- 10
   escreva(a,",",b,",",c)
Fimalgoritmo
*/
