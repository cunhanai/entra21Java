package lista3;

public class NegociacaoAcoes {
	public static void main(String[] args) {
		double taxa, valorAcaoCompra, valorAcaoVenda, valorCompra, valorVenda, comissaoCompra, comissaoVenda, gastosCompra, lucro, prejuizo;
		int qtdeAcoes = 1000;
		
		taxa = 0.02;
		valorAcaoCompra = 32.87;
		valorAcaoVenda = 33.92;
		
		valorCompra = qtdeAcoes * valorAcaoCompra;
		comissaoCompra = valorCompra * taxa;
		gastosCompra = valorCompra + comissaoCompra;
		
		valorVenda = qtdeAcoes * valorAcaoVenda;
		comissaoVenda = valorVenda * taxa;
		
		lucro = valorVenda - (gastosCompra + comissaoVenda);
		
		System.out.println("Quantia que Jos� pagou pelas a��es: R$ " + valorCompra);
		System.out.println("Valor de comiss�o pago ao corretor na compra: R$ " + comissaoCompra);
		System.out.println("Quantia que Jos� vendeu as a��es: R$ " + valorVenda);
		System.out.println("Valor de comiss�o pago ao corretor na venda: R$ " + comissaoVenda);
		
		if (lucro > 0) {
			System.out.println("Jos� obteve um lucro de: R$ " + lucro);
		}
		else {
			prejuizo = lucro * (-1);
			System.out.println("Jos� obteve um preju�zo de: R$ " + prejuizo);
		}
	}
}



/*
Algoritmo "ex 18 - nagocia��o de a��es"
Var
   taxa, comissaoCompra, comissaoVenda, valorCompra, valorVenda, gastosCompra, lucro, prejuizo: real;
   qtdeAcoes: inteiro;
   
Inicio
   taxa <- 0.02
   qtdeAcoes <- 1000
   valorCompra <- qtdeAcoes * 32.87
   comissaoCompra <- valorCompra * taxa
   gastosCompra <- valorCompra + comissaoCompra
   
   valorVenda <- qtdeAcoes * 33.92
   comissaoVenda <- valorVenda * taxa
   
   lucro <- valorVenda - (gastosCompra + comissaoVenda)

   escreval("Quantia que Jos� pagou pelas a��es: R$", valorCompra)
   escreval("Valor de comiss�o pago ao corretor na compra: R$", comissaoCompra)
   escreval("Quantia que Jos� vendeu as a��es: R$", valorVenda)
   escreval("Valor de comiss�o pago ao corretor na venda: R$", comissaoVenda)
   se lucro > 0 entao
      escreval("Lucro de Jos� com essas transa��es: ", lucro)
   senao
        prejuizo <- lucro * (-1)
        escreval("Jos� obteve um preju�zo de: R$", prejuizo)
   fimse
Fimalgoritmo
*/