package lista3;
import java.util.Scanner;

public class LucroDespesa {

	public static void main(String[] args) {
		double receita, despesas, lucro, percentual;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Valor da receita: R$ ");
		receita = sc.nextDouble();
		System.out.print("Valor das despesas: R$ ");
		despesas = sc.nextDouble();
		
		lucro = receita - despesas;
		percentual = (despesas * 100) / receita;
		System.out.println("Lucro: R$ " + lucro);
		System.out.println("Percentual de despesas: " + percentual + "%");
		
		sc.close();
	}

}


/*
Algoritmo "ex 16 - lucro e percentua de despesas"
Var
receita, despesas, lucro, percentual: real

Inicio
escreva("Valor da receita: R$ ")
leia(receita)
escreva("Valor das despesas: R$ ")
leia(despesas)

lucro <- receita - despesas
percentual <- (despesas * 100) / receita

escreval("Lucro: R$", lucro)
escreval("Percentual de despesas: ", percentual, "%")
Fimalgoritmo
*/
