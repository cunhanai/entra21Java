package lista3;
import java.util.Scanner;

public class DiasVida {

	public static void main(String[] args) {
		int idade, dias;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		System.out.print("Idade: ");
		idade = Integer.parseInt(sc.nextLine());
		
		dias = idade * 365;
		
		System.out.println(nome + " j� viveu aproximadamente " + dias + " dias.");
		
		sc.close();
	}

}


/*
Algoritmo "ex 17 - dias de vida"
Var
nome: caractere
idade, dias: inteiro

Inicio
escreva("Nome: ")
leia(nome)
escreva("Idade: ")
leia(idade)

dias <- idade * 365

escreva(nome, " j� viveu aproximadamente", dias, " dias")
Fimalgoritmo
*/
