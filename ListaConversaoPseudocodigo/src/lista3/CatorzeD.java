package lista3;

public class CatorzeD {
	
	public static void main(String[] args) {
		int a, b;
		
		a = 10;
		b = a + 1;
		a = b + 1;
		b = a + 1;
		System.out.println(a);
		
		a = b + 1;
		System.out.println(a + ", " + b);
	}
}



/*
Algoritmo "ex 14.d)"
Var
   a, b: inteiro
Inicio
   a <- 10
   b <- a + 1
   a <- b + 1
   b <- a + 1
   escreval(a)
   
   a <- b + 1
   escreval(a,",",b)
Fimalgoritmo
*/
