package lista3;

public class CatorzeC {
	
	public static void main(String[] args) {
		int a, b, c;
		
		a = 10;
		b = 20;
		c = a;
		b = c;
		a = b;
		
		System.out.println(a + ", " + b + ", " + c);
	}
}




/*
Algoritmo "ex 14.c)"
Var
   a, b, c: inteiro
Inicio
   a <- 10
   b <- 20
   c <- a
   b <- c
   a <- b
   escreva(a,",",b,",",c)
Fimalgoritmo
*/
