package lista3;
import java.util.Scanner;

public class ConversaoHorasMinutos {

	public static void main(String[] args) {
		int hora, minuto, segundo;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite uma hora qualquer: ");
		hora = sc.nextInt();
		
		minuto = hora * 60;
		segundo = hora * 3600;
		
		System.out.println(hora + " hora(s) corresponde a " + minuto + " minutos e " + segundo + " segundo");
		
		sc.close();
	}

}


/*
Algoritmo "ex 19 - convers�o hora para minutos e segundos"
Var
hora, minutos, segundos: inteiro

Inicio
escreva("Digite uma hora qualquer: ")
leia(hora)

minutos <- hora * 60
segundos <- hora * 3600

escreval()
escreval(hora, " hora(s) corresponde a:")
escreval(minutos, " minutos")
escreval(segundos, " segundos")
Fimalgoritmo
*/
