package lista3;
import java.util.Scanner;
import java.lang.Math;

public class AreaQuadrado {
	
	public static void main(String[] args) {
		double lado, area;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("�rea do quadrado: ");
		lado = sc.nextDouble();
		
		area = Math.pow(lado, 2);
		System.out.println("�rea: " + area);
		
		sc.close();
	}
}


/*
Algoritmo "ex 15 - �rea de um quadrado"
Var
lado: real
area: real

Inicio
escreva("�rea do quadrado: ")
leia(lado)

area <- lado^2
escreva("�rea: ", area)
Fimalgoritmo
*/