package lista3;

public class TrocaValorVariaveis {

	public static void main(String[] args) {
		int a = 10, b = 20, aux;
		
		aux = a;
		a = b;
		b = aux;
		
		System.out.println("O valor de A �: " + a);
		System.out.println("O valor de B �: " + b);
	}

}



/*
Algoritmo "ex 13 - troca de valor entre variaveis"
Var
a, b, aux: inteiro
Inicio
a <- 10
b <- 20

aux <- a
a <- b
b <- aux

escreval("O valor de A �: ", a)
escreval("O valor de B �: ", b)
Fimalgoritmo
*/