package lista3;

public class CatorzeA {
	
	public static void main(String[] args) {
		int a = 10, b = 20;
		System.out.println(b);
		
		b = 5;
		System.out.println(a + ", " + b);
	}
}

/*
Algoritmo "ex 14.a)"
Var
   a, b: inteiro
Inicio
   a <- 10
   b <- 20
   escreval(b)
   
   b <- 5
   escreval(a, ",", b)
Fimalgoritmo
*/