package lista3;

public class CatorzeF {
	
	public static void main(String[] args) {
		int x, y, z;
		
		x = 1;
		y = 2;
		z = y - x;
		System.out.println(z);
		
		x = 5;
		y = x + z;
		System.out.println(x + ", " + y + ", " + z);
	}
}


/*
Algoritmo "ex 14.f)"
Var
   x, y, z: inteiro
Inicio
   x <- 1
   y <- 2
   z <- y - x
   escreval(z)
   
   x <- 5
   y <- x + z
   
   escreval(x,",",y,",",z)
Fimalgoritmo
*/
