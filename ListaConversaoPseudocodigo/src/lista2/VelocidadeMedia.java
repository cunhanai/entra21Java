package lista2;
import java.util.Scanner;

public class VelocidadeMedia {

	public static void main(String[] args) {
		double distancia, tempo, velocidade;
		String nomePiloto;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nome do piloto: ");
		nomePiloto = sc.nextLine();
		System.out.print("Dist�ncia (em horas): ");
		distancia = sc.nextDouble();
		System.out.print("Tempo: ");
		tempo = sc.nextDouble();
		
		velocidade = distancia / tempo;
		
		System.out.println("A velocidade m�dia de " + nomePiloto + " foi " + velocidade + "km/h");
		
		sc.close();
	}

}




/*
Algoritmo "ex 8 - calculo velocidade"
Var
piloto: caractere
distancia: real
tempo_p: real
velocidade: real

Inicio
leia(piloto)
leia(distancia)
leia(tempo_p)

velocidade <- distancia / tempo_p

escreva("A velocidade m�dia de ", piloto, " foi ", velocidade, "km/h")

Fimalgoritmo
*/