package lista2;
import java.util.Scanner;

public class SalarioFuncionario {

	public static void main(String[] args) {
		double horasTrab, valorHora, acresPorFilho, salario, salarioTotal, acrescimo;
		int qtdeFilhos;
		String nomeFunc;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nome do funcion�rio: ");;
		nomeFunc = sc.nextLine();
		System.out.print("Quantidade de horas trabalhadas por " + nomeFunc + ": ");
		horasTrab = sc.nextDouble();
		System.out.print("Valor que " + nomeFunc + " recebe por hora: ");
		valorHora = sc.nextDouble();
		System.out.print("Quantidade de filhos que " + nomeFunc + " possui: ");
		qtdeFilhos = sc.nextInt();
		
		acresPorFilho = 0.03;
		salario = horasTrab * valorHora;
		acrescimo = acresPorFilho * qtdeFilhos * salario;
		salarioTotal = salario + acrescimo;
		
		System.out.println("O sal�rio final de " + nomeFunc + " � R$ " + salarioTotal);
		
		sc.close();
	}

}



/*
Algoritmo "ex 10 - calculo salario com filho"
Var
nomeFuncionario: caractere
horasTrabalhadas: real
valorHora: real
qtdeFilhos: inteiro
acrescimoPorFilho: real
salario: real
salarioTotal: real

Inicio
escreva("Nome do funcion�rio: ")
leia(nomeFuncionario)
escreva("N�mero de horas trabalhadas por ", nomeFuncionario, ": ")
leia(horasTrabalhadas)
escreva("Valor que ", nomeFuncionario, " recebe por hora: ")
leia(valorHora)
escreva("Quantidade de filhos que ", nomeFuncionario, " possui: ")
acrescimoPorFilho <- 0.03

salario <- (horasTrabalhadas * valorHora)
salarioTotal <- salario + (salario * acrescimoPorFilho)

escreva("O sal�rio final de  ", nomeFuncionario, " � R$ ", salarioTotal)

Fimalgoritmo
*/