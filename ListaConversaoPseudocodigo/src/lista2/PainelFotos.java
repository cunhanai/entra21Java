package lista2;
import java.util.Scanner;

public class PainelFotos {
	
	public static void main(String[] args) {
		float valorPacote, valorAvulso;
		double totalPacote, totalAvulso, avulsoComPacote;
		int qtdeFotos, qtdePacote, qtdeAvulso;
		Scanner sc = new Scanner(System.in);
		
		valorPacote = 44;
		valorAvulso = 0.7f;
		
		System.out.print("Quantidade de fotos que deseja revelar: ");
		qtdeFotos = sc.nextInt();
		
		totalAvulso = qtdeFotos * valorAvulso;
		System.out.println("Valor das fotos avulsas: R$ " + totalAvulso);
		
		if (qtdeFotos < 100) {
			System.out.println("N�o h� op��es dispon�veis para a quantidade desejada.");
		}
		else if ((qtdeFotos % 100) == 0) {
			qtdePacote = (int) qtdeFotos / 100;
			totalPacote = qtdePacote * valorPacote;
			System.out.println("Valor das fotos em " + qtdePacote + " pacote(s): R$ " + totalPacote);
		}
		else {
			qtdePacote = (int) (qtdeFotos - qtdeFotos % 100)/100;
			totalPacote = qtdePacote * valorPacote;
			qtdeAvulso = (int) qtdeFotos % 100;
			avulsoComPacote = qtdeAvulso * valorAvulso + totalPacote;
			
			System.out.println("Valor com " + qtdePacote + " pacote(s) e " + qtdeAvulso + " fotos avulsas: R$ " + avulsoComPacote);
		}
		
		sc.close();
	}
}


/*
Algoritmo "ex 12 - painel de fotos"
Var
valorPacote: real
valorAvulso: real
qtdeFotos: inteiro
totalPacote: real
totalAvulso: real
avulsoComPacote: real
qtdePacotes: real
qtdeAvulsas: real

Inicio
valorPacote <- 44
valorAvulso <- 0.7

escreva("Quantidade de fotos que deseja revelar: ")
leia(qtdeFotos)
totalAvulso <- qtdeFotos * valorAvulso

se (qtdeFotos < 100) entao
   escreval("Valor das fotos avulsas R$", totalAvulso)
   escreval("N�o h� op��es de pacote dispon�veis para a quantidade desejada.")
senao
   se (qtdeFotos % 100) = 0 entao
      totalPacote <- (qtdeFotos / 100) * valorPacote
      escreval("Valor das fotos avulsas: R$", totalAvulso)
      escreval("Valor das fotos em pacote: R$", totalPacote)
   senao
        qtdePacotes <- (qtdeFotos - qtdeFotos % 100)/100
        totalPacote <- qtdePacotes * 44
        qtdeAvulsas <- qtdeFotos % 100
        avulsoComPacote <- qtdeAvulsas * 0.7 + totalPacote
        escreval("Valor de todas as fotos avulsas: R$", totalAvulso)
        escreval("Valor com ", qtdePacotes, " pacotes e ", qtdeAvulsas," fotos avulsas:  R$", avulsoComPacote)
   fimse
fimse
Fimalgoritmo
*/