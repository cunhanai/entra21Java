package lista2;
import java.util.Scanner;

public class CompraFlores {

	public static void main(String[] args) {
		int qtdeRosas, qtdeTulipas;
		double valorRosas, valorTulipas, precoRosas, precoTulipas;
		Scanner sc = new Scanner(System.in);
		
		valorRosas = 2.8;
		valorTulipas = 4.2;
		
		System.out.print("Quantidade de rosas: ");
		qtdeRosas = sc.nextInt();
		System.out.print("Quantidade de tulipas: ");
		qtdeTulipas = sc.nextInt();
		
		precoRosas = qtdeRosas * valorRosas;
		precoTulipas = qtdeTulipas * valorTulipas;
		
		System.out.println("Pre�o das rosas: R$ " + precoRosas);
		System.out.println("Pre�o das tulipas: R$ " + precoTulipas);
		
		sc.close();
	}

}



/*
Algoritmo "ex 11 - quantas flores comprar"
Var
qtdeRosas: inteiro
qtdeTulipas: inteiro
totalRosas: real
totalTulipas: real

Inicio
escreva("Digite a quantidade de rosas: ")
leia(qtdeRosas)
escreva("Digite a quantidade de tulipas: ")
leia(qtdeTulipas)

totalRosas <- qtdeRosas * 2.8
totalTulipas <- qtdeTulipas * 4.2

escreval("Total de Rosas: R$ ", totalRosas)
escreval("Total de Tulipas: R$ ", totalTulipas)
Fimalgoritmo
*/