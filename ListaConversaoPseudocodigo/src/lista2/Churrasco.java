package lista2;
import java.util.Scanner;

public class Churrasco {

	public static void main(String[] args) {
		int qtdePessoas, consumoCerveja;
		double consumoCarne, precoQuiloCarne, precoCerveja, calculoCarne, calculoCerveja;
		
		consumoCarne = 500;
		consumoCerveja = 6;
		precoQuiloCarne = 18;
		precoCerveja = 1.7;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Quantidade de pessoas: ");
		qtdePessoas = sc.nextInt();
		
		calculoCarne = (consumoCarne / 1000) * precoQuiloCarne * qtdePessoas;
		calculoCerveja = consumoCerveja * precoCerveja * qtdePessoas;
		
		System.out.println("A carne sair� entorno de R$ " + calculoCarne);
		System.out.println("A cerveja sair� entorno de R$ " + calculoCerveja);
		
		sc.close();
	}

}


/*
Algoritmo "ex 9 - churrasco"
Var
qtdePessoas: inteiro
consumoCarne: inteiro
consumoCerveja: inteiro
quiloCarne: inteiro
precoCerveja: real
calculoCarne: real
calculoCerveja: real

Inicio
escreva("Quantidade de pessoas: ")
leia(qtdePessoas)

consumoCarne <- 500
consumoCerveja <- 6
quiloCarne <- 18
precoCerveja <- 1.70

calculoCarne <- (consumoCarne/1000) * quiloCarne * qtdePessoas
calculoCerveja <- consumoCerveja * precoCerveja * qtdePessoas

escreval("A carne sair� entorno de R$ ", calculoCarne)
escreval("A cerveja sair� entorno de R$ ", calculoCerveja)

Fimalgoritmo
*/