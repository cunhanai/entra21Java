package lista2;
import java.util.Scanner;

public class CelsiusToFahrenheit {
	
	public static void main(String[] args) {
		double celsius, fahrenheit;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Graus Celsius(�C): ");
		celsius = sc.nextDouble();
		
		fahrenheit = (1.8 * celsius) + 32;
		System.out.println(fahrenheit + "�F");
		
		sc.close();
	}
}





/*
Var
C: inteiro
F: real
Inicio

escreva("Graus em Celsius: ")
leia(C)

F <- (1.8*C)+32

escreva(F, "�F")

Fimalgoritmo
*/