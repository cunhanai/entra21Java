package lista2;
import java.util.Scanner;

public class Buffet {

	
	public static void main(String[] args) {
		double pesoPrato, pesoComida, precoKg, total;
		pesoPrato = 230;
		precoKg = 20;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Peso da refei��o (contando o peso do prato): ");
		pesoComida = (sc.nextDouble() - pesoPrato) / 1000;
		
		total = pesoComida * precoKg;
		System.out.println("Peso: " + pesoComida + " kg\nValor a pagar: " + total);
		
		sc.close();
	}

}



/*
Algoritmo "ex 7 - calculo buffet a quilo"
Var
peso: inteiro
prato: inteiro
preco: real
total: real

Inicio
	 escreva("Peso da refei��o (contando o peso do prato): ")
leia(peso)
prato <- 230
preco <- 20

total <- (peso - prato)/1000 * preco

escreva("O valor a ser pago � de R$ ", total)

Fimalgoritmo
*/