package lista2;
import java.util.Scanner;

public class DevolucaoLivro {
	public static void main(String[] args) {
		double valor, multa = 2.5;
		int diasAtraso;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Dias de atraso na devolu��o: ");
		diasAtraso = sc.nextInt();
		
		valor = diasAtraso * multa;;
		System.out.println("A multa para " +  diasAtraso + " dias de atraso � de R$ " + valor);
		
		sc.close();
	}
}




/*
Algoritmo "ex 6 - atraso devolu��o de livro"
Var
dias_atraso: inteiro
multa: real
valor: real

Inicio
escreva("Dias de atraso na devolu��o: ")
leia(dias_atraso)
multa <- 2.5

valor <- dias_atraso * multa

escreva("A multa � de: R$ ",valor)
Fimalgoritmo
*/