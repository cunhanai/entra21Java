package lista5;

import java.util.Scanner;

public class Questao14CreditoEspecial {

	public static void main(String[] args) {
		double saldoMedio, valorCredito;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Saldo m�dio do cliente: ");
		saldoMedio = sc.nextDouble();
		
		if (saldoMedio <= 200) {
			System.out.println("Nenhum cr�dito.");
		}
		else if (saldoMedio >= 201 && saldoMedio <= 400) {
			valorCredito = saldoMedio * 0.2;
			System.out.println("20% do valor do saldo m�dio: " + valorCredito);
		}
		else if (saldoMedio >= 401 && saldoMedio <= 600) {
			valorCredito = saldoMedio * 0.3;
			System.out.println("30% do valor do saldo m�dio: " + valorCredito);
		}
		else {
			valorCredito = saldoMedio * 0.4;
			System.out.println("40% do valor do saldo m�dio: " + valorCredito);
		}
		
		sc.close();

	}

}
