package lista5;
import java.util.Scanner;

public class Questao1ParImpar {
	
	/*
	 * Quest�o_1:  Elabore  um  PROGRAMA,  que  dado  um  n�mero inteiro,
	 * positivo, verifique e exiba se ele � par ou �mpar (Obs.: um n�mero
	 * par � divis�vel por 2, ou seja, o resto da divis�o por 2 � igual a zero).
	 */

		public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Digite um n�mero inteiro positivo: ");
			int numero = sc.nextInt();
			
			if ((numero % 2) == 0) {
				System.out.println("par");
			}
			else {
				System.out.println("impar");
			}
			
			
			
			String result = ((numero % 2) == 0) ? "par" : "�mpar";
			System.out.println(result);
			
			
			
			
			switch (numero % 2) {
			case 0:
				System.out.println("par");
				break;
			default:
				System.out.println("impar");
			}
			
			
			sc.close();
		}
}
