package lista5;

import java.util.Scanner;

public class Questao5Nadador {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String classificacao;
		
		System.out.print("Idade do nadador: ");
		int idade = sc.nextInt();
		
		if (idade < 5) {
			classificacao = "inválidada/indisponível";
		}
		else if (idade >= 5 && idade <= 7) {
			classificacao = "Infantil A";
		}
		else if (idade >= 8 && idade <= 10) {
			classificacao = "Infantil B";
		}
		else if (idade >= 11 && idade <= 13) {
			classificacao = "Juvenil A";
		}
		else if (idade >= 14 && idade <= 17) {
			classificacao = "Juvenil B";
		}
		else {
			classificacao = "Adulto";
		}
			
		System.out.println("Classificação: " + classificacao);
		
		sc.close();
	}

}
