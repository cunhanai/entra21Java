package lista5;

import java.util.Scanner;

public class Questao8Combustivel {

	public static void main(String[] args) {
		char combustivel;
		double litros, total = 0;
		boolean trocaOleo = false;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Combust�vel \t Valor litro");
		System.out.println("[1] Gasolina \t R$ 2,53");
		System.out.println("[2] Etanol \t R$ 2,09");
		System.out.println("[3] Diesel \t R$ 1,92");
		
		System.out.print("\nTipo de combust�vel: ");
		combustivel = sc.next().charAt(0);
		System.out.print("Quantidade de litros: ");
		litros = sc.nextDouble();
		
		switch (combustivel) {
		case '1':
			total = litros * 2.53;
			break;
		case '2':
			total = litros * 1.92;
			break;
		case '3':
			total = litros * 2.09;
			trocaOleo = (litros > 30) ? true : false;
			break;
		default:
			System.out.println("C�digo errado! Por favor, digite um c�digo v�lido!");
			break;
		}
		
		System.out.println("\nValor total: " + total);
		
		if (trocaOleo == true) {
			System.out.println("Voc� ganhou uma troca de �leo!");
		}
		
		sc.close();
	}

}
