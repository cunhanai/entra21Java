package lista5;

import java.util.Scanner;

public class Questao11Calculadora {

	public static void main(String[] args) {
		double valor1, valor2;
		char operacao;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Valor 1: ");
		valor1 = sc.nextDouble();
		System.out.print("Valor 2: ");
		valor2 = sc.nextDouble();
		
		System.out.println("\nOpera��es v�lidas: ");
		System.out.println("+  adi��o");
		System.out.println("-  subtra��o");
		System.out.println("*  multiplica��o");
		System.out.println("/  divis�o");
		
		System.out.print("\n\nOpera��o: ");
		operacao = sc.next().charAt(0);
		
		switch (operacao) {
		case '+':
			System.out.println(valor1 + valor2);
			break;
		case '-':
			System.out.println(valor1 - valor2);
			break;
		case '*':
			System.out.println(valor1 * valor2);
			break;
		case '/':
			System.out.println(valor1 / valor2);
			break;
		default:
			System.out.print("Opera��o inv�lida.");
			break;
		}
		
		sc.close();
	}

}
