package lista5;

import java.util.Scanner;

public class Questao9CaixaDeFruta {

	public static void main(String[] args) {
		char fruta;
		double valorUnitario, precoCaixa = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Caixa de \t Unidades");
		System.out.println("[O] Laranja \t 60");
		System.out.println("[L] Lim�o \t 80");
		System.out.println("[S] Morango \t 20");
		
		System.out.print("\nFruta: ");
		fruta = sc.next().charAt(0);
		System.out.print("Valor unit�rio: ");
		valorUnitario = sc.nextDouble();
		
		switch (fruta) {
		case 'O':
			precoCaixa = valorUnitario * 60;
			System.out.println("Pre�o da caixa de laranja: " + precoCaixa);
			break;
		case 'L':
			precoCaixa = valorUnitario * 80;
			System.out.println("Pre�o da caixa de lim�o: " + precoCaixa);
			break;
		case 'S':
			precoCaixa = valorUnitario * 20;
			System.out.println("Pre�o da caixa de morango: " + precoCaixa);
			break;
		default:
			System.out.print("Comando errado! Por favor, digite uma das letras dispon�veis em mai�sculo.");
			break;
		}
		
		sc.close();
	}

}
