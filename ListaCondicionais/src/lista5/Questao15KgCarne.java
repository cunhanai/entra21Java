package lista5;

import java.util.Scanner;

public class Questao15KgCarne {

	public static void main(String[] args) {
		int tipoCarne, formaPagamento;
		double qtde, valor, preco, desconto, valorTotal;
		boolean compra = false;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("--------------------------------------------");
		System.out.println("Tipo carne \t\t Pre�o");
		System.out.println("\t\tQtd < 5kg\tQtd > 5kg");
		System.out.println("[1]Fil� duplo \t 4,9 \t\t 5,8");
		System.out.println("[2]Alcatra \t 5,9 \t\t 6,8");
		System.out.println("[3]Picanha \t 6,9 \t\t 7,8");
		System.out.println("--------------------------------------------\n");
		
		System.out.println("Tipo de carne: ");
		tipoCarne = sc.nextInt();
		System.out.println("Quantidade (kg): ");
		qtde = sc.nextDouble();
		
		switch (tipoCarne) {
		case 1:
			if (qtde < 5) {
				preco = 4.9;
			}
			else {
				preco = 5.8;
			}
			
			compra = true;
			break;
		case 2:
			if (qtde < 5) {
				preco = 5.9;
			}
			else {
				preco = 6.8;
			}
			
			compra = true;
			break;
		case 3:
			if (qtde < 5) {
				preco = 6.9;
			}
			else {
				preco = 7.8;
			}
			
			compra = true;
			break;
		default:
			compra = false;
			preco = 0;
			valor = 0;
			break;
		}
		
		valor = qtde * preco;
		
		if (compra == true) {
			System.out.println("\n" + qtde + " * " + preco + " = " + valor);
			System.out.println("\nForma de pagamento:");
			System.out.println("[1] Cart�o");
			System.out.println("[2] Dinheiro");
			System.out.println("[3] Pix");
			
			System.out.print("\nQual a forma de pagamento? ");
			formaPagamento = sc.nextInt();
			
			if (formaPagamento == 1) {
				desconto = valor * 0.05;
				valorTotal = valor - desconto;
				System.out.println("Voc� obteve um desconto de " + desconto + " (5%)");
			}
			else {
				valorTotal = valor;
			}
			
			System.out.println("Valor total: " + valorTotal);
		}
		else {
			System.out.println("Tipo de carne inv�lido.");
		}
		
		sc.close();
	}

}
