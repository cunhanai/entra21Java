package lista5;

import java.util.Scanner;

public class Questao10Notas {

	public static void main(String[] args) {
		int $1, $10, $100, valorCompra, valorPagamento, troco, moduloCem, centena, dezena, unidade;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Valor da compra: ");
		valorCompra = sc.nextInt();
		System.out.print("Valor do pagamento: ");
		valorPagamento = sc.nextInt();
		
		if (valorPagamento < valorCompra) {
			System.out.println("Pagamento Negado");
		}
		else {
			troco = valorPagamento - valorCompra;
			
			moduloCem = troco % 100;
			centena = troco - moduloCem; 
			$100 = (troco >= 100) ? centena / 100 : 0;
			
			dezena = moduloCem - (moduloCem % 10);
			$10 = (dezena >= 10) ? dezena / 10 : 0;
			
			unidade = troco - centena - dezena;
			$1 = (unidade >= 1) ? unidade : 0;
			
			System.out.println("\nTroco = " + troco);
			System.out.println($100 + " notas de R$ 100");
			System.out.println($10 + " notas de R$ 10");
			System.out.println($1 + " notas de R$ 1");
		}
		
		sc.close();
	}

}
