package lista5;

import java.util.Scanner;

public class Questao16Calculadora {

	public static void main(String[] args) {
		double valor1, valor2, res;
		char operacao;
		boolean valido = false;
		String posNeg, parImp;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Valor 1: ");
		valor1 = sc.nextDouble();
		System.out.print("Valor 2: ");
		valor2 = sc.nextDouble();
		
		System.out.println("\nOpera��es v�lidas: ");
		System.out.println("+  adi��o");
		System.out.println("-  subtra��o");
		System.out.println("*  multiplica��o");
		System.out.println("/  divis�o");
		
		System.out.print("\n\nOpera��o: ");
		operacao = sc.next().charAt(0);
		
		switch (operacao) {
		case '+':
			res = valor1 + valor2;
			valido = true;
			break;
		case '-':
			res = valor1 - valor2;
			valido = true;
			break;
		case '*':
			res = valor1 * valor2;
			valido = true;
			break;
		case '/':
			res = valor1 / valor2;
			valido = true;
			break;
		default:
			valido = false;
			res = 0;
			break;
		}
		
		if (valido == true) {
			parImp = ((res % 2) == 0) ? "par" : "�mpar";
			posNeg = (res == 0) ? "neutro" : (res > 0) ? "positivo" : "negativo";
			
			System.out.printf("O n�mero � %s e %s", parImp, posNeg);
		}
		else {
			System.out.println("Opera��o inv�lida!");
		}
		
		sc.close();
	}
}
