package lista5;

import java.util.Scanner;

public class Questao18VogalConsoante {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char letra;
		
		System.out.print("Letra: ");
		letra = Character.toLowerCase(sc.next().charAt(0));
		
		if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
			System.out.println("Vogal");
		}
		else {
			System.out.println("Consoante");
		}
		
		sc.close();
	}

}
