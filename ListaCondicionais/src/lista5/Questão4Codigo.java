package lista5;

import java.util.Scanner;

public class Quest�o4Codigo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String classificacao;
		
		System.out.print("C�digo do produto: ");
		int codigo = sc.nextInt();
		
		if (codigo == 1) {
			classificacao = "alimento n�o-perec�vel";
		}
		else if (codigo >= 2 && codigo <= 4) {
			classificacao = "alimento perec�vel";
		}
		else if (codigo == 5 || codigo == 6) {
			classificacao = "vestu�rio";
		}
		else if (codigo == 7) {
			classificacao = "higiene pessoal";
		}
		else if (codigo >= 8 && codigo <= 15) {
			classificacao = "limpeza e utens�lios dom�sticos";
		}
		else {
			classificacao = "inv�lido";
		}
		
		if (classificacao == "inv�lido") {
			System.out.println("C�digo inv�lido!");
		}
		else {
			System.out.println("Classe do produto: " + classificacao);
		}
		
		sc.close();
	}

}
