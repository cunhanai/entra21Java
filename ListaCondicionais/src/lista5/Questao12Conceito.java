package lista5;

import java.util.Scanner;

public class Questao12Conceito {

	public static void main(String[] args) {
		double n1, n2, n3, mediaExercicios, media;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nota 1: ");
		n1 = sc.nextDouble();
		System.out.print("Nota 2: ");
		n2 = sc.nextDouble();
		System.out.print("Nota 3: ");
		n3 = sc.nextDouble();
		System.out.print("M�dia dos exerc�cios: ");
		mediaExercicios = sc.nextDouble();
		
		media = (n1 + n2*2 + n3*3 + mediaExercicios)/7;
		
		System.out.println("M�dia de aproveitamento: " + media);
		
		if (media >= 9) {
			System.out.println("Conceito A");
		}
		else if (media >= 7.5) {
			System.out.println("Conceito B");
		}
		else if (media >= 6) {
			System.out.println("Conceito C");
		}
		else {
			System.out.println("Conceito D");
		}
		
		sc.close();
	}

}
