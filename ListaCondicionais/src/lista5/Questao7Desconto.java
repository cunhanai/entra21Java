package lista5;

import java.util.Scanner;

public class Questao7Desconto {
	
	public static void main(String[] args) {
		double valor, valorFinal, desconto = 0.1;
		char formaPagamento;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Valor: ");
		valor = sc.nextDouble();
		
		System.out.print("\nForma de pagamento");
		System.out.print("\n\n  [C] Cart�o");
		System.out.print("\n  [B] Boleto");
		System.out.print("\n  [D] Dinheiro");
		System.out.print("\n  [P] Pix");
		System.out.print("\n  [T] Transfer�ncia");
		System.out.print("\n\n Digite a letra da forma de pagamento: ");
		formaPagamento = sc.next().charAt(0);
		
		if ((formaPagamento == 'D' || formaPagamento == 'd') && valor >= 100) {
			valorFinal = valor - (valor * desconto);
			System.out.println("\nDesconto: 10% \nValor final: " + valorFinal);
		}
		else {
			System.out.println("\nEsta compra n�o possui desconto. \nValor final: " + valor);
		}
		
		sc.close();
	}
}
