package lista5;

import java.util.Scanner;

public class Questao3PesoIdeal {
	/*
	 * Quest�o_3: Tendo como dados de entrada a altura e o sexo de  uma  pessoa, construa 
	 * um PROGRAMA que  calcule  seu peso ideal, utilizando as seguintes f�rmulas:
	 * Para homens: (72.7*alt)-58;
	 * Para mulheres: (62.1*alt)-44.7;
	 */
	
	public static void main(String[] args) {
		double pesoIdeal, altura;
		String sexo;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Altura: ");
		altura = sc.nextDouble();
		System.out.print("Sexo (feminino/masculino): ");
		sexo = sc.next();
		
		switch (sexo) {
		case "feminino":
			pesoIdeal = (62.1 * altura) - 44.7;
			System.out.println("Seu peso ideal � " + pesoIdeal);
			break;
		case "masculino":
			pesoIdeal = (72.7 * altura) - 58;
			System.out.println("Seu peso ideal � " + pesoIdeal);
			break;
		default:
			System.out.println("Op��o inv�lida!");
		}
		
		sc.close();
	}
}
