package lista5;

import java.util.Scanner;

public class Questao13Triangulo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double l1, l2, l3;
		boolean verif1, verif2, verif3;
		
		System.out.print("Lado 1: ");
		l1 = sc.nextDouble();
		System.out.print("Lado 2: ");
		l2 = sc.nextDouble();
		System.out.print("Lado 3: ");
		l3 = sc.nextDouble();
		
		verif1 = ((l1 + l2) > l3) ? true : false;
		verif2 = ((l2 + l3) > l1) ? true : false;
		verif3 = ((l1 + l3) > l2) ? true : false;
		
		if (verif1 == true && verif2 == true && verif3 == true) {
			if (l1 == l2 && l1 == l3) {
				System.out.println("Tri�ngulo equil�tero.");
			}
			else if (l1 == l2 || l1 == l3 || l2 == l3) {
				System.out.println("Tri�ngulo is�celes.");
			}
			else {
				System.out.println("Tri�ngulo escaleno.");
			}
		}
		else {
			System.out.println("N�o forma tri�ngulo.");
		}
		
		sc.close();
	}

}
