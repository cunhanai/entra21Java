package lista5;
import java.util.Scanner;

public class Questao6IMC {

	public static void main(String[] args) {
		double peso, altura, imc;
		String condicao;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Peso: ");
		peso = sc.nextDouble();
		System.out.print("Altura: ");
		altura = sc.nextDouble();
		
		imc = peso / (altura * altura);
		
		if (imc < 18.5) {
			condicao = "abaixo do peso";
		}
		else if (imc >= 18.5 && imc < 25) {
			condicao = "peso normal";
		}
		else if (imc >= 25 && imc <= 30) {
			condicao = "acima do peso";
		}
		else {
			condicao = "obeso";
		}
		
		System.out.println("Condi��o: " + condicao);
		
		sc.close();
	}

}
