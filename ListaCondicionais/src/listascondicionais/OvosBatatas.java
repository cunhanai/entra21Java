package listascondicionais;

public class OvosBatatas {

	public static void main(String[] args) {
		int ovos = 6;
		
		boolean batata = true;
		int batatas = (batata == true) ? 9 : 0;
		
		System.out.println("Levei para casa " + ovos + " ovos e " + batatas + " batatas");
	}

}
