package listascondicionais;

public class SalarioIfElse {

	public static void main(String[] args) {
		int salario = 1000, bonus;
		
		if (salario > 1000) {
			bonus = 10;
		}
		else {
			bonus = 15;
		}
		
		System.out.println("B�nus de " + bonus + "%");
	}

}
