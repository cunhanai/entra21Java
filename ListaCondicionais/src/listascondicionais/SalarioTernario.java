package listascondicionais;

public class SalarioTernario {

	public static void main(String[] args) {
		int salario = 1000, bonus;
		
		bonus = (salario > 1000) ? 10 : 15;
		
		System.out.println("B�nus de " + bonus + "%");
	}

}
